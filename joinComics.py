import os
import shutil
import zipfile
import sys
import zlib

try:
	import rarfile
except:
	print 'You need to install the rarfile package for Python.'

downDir = 'temp'
counter = 1

def extractComic(filename):
	print 'Extracting ' + '"' + filename + '"'
	outPut = downDir + '/' + filename[:-4]
	os.makedirs(outPut)

	try:
		rf = rarfile.RarFile(filename)
		rf.extractall(outPut)
		rf.close()
	except:
		zf = zipfile.ZipFile(filename)
		zf.extractall(outPut)
		zf.close()

def printHelp():
	print 'Usage: joinComics.py "combinedComics.cbz" [0-9] [0-9]'
	print ''
	print 'Combines .CBR and .CBZ files together.'
	print 'This script has 3 arguments. Only one is requried'
	print 'Requried argument:'
	print ' filename\tWhat you want your file to be called. It will be in zip format, so .cbz is best'
	print ''
	print 'Optional arguments:'
	print ' 0-9 0-9\tIf you only want to combine a certain section of comics together, specify here. Ex. 11 20 for the 11-20th comics in the folder.'
	quit()

def removeCopy():
	if os.path.exists(sys.argv[1]):
		os.remove(sys.argv[1])

if len(sys.argv) < 2:
	print 'You must provide a file name to save your CBZ file: Ex. comic.cbz'
	printHelp()
elif len(sys.argv) == 4:
	try:
		removeCopy()

		startNum = int(sys.argv[2])
		endNum = int(sys.argv[3])

		if startNum < endNum:
			print "We'll try and combine all the comics between " + str(startNum) + ' and ' + str(endNum) + '.'
		else:
			print 'Please make sure your starting number is lower than the ending number.'
			quit()

	except:
		print 'Those are not valid numbers.'
		quit()
elif len(sys.argv) == 2:
	removeCopy()

	if sys.argv[1] == '--help' or sys.argv[1] == '-help' or sys.argv[1] == 'help' or sys.argv[1] == '/?'or sys.argv[1] == '--h' or sys.argv[1] == '-h':
		printHelp()
	else:
		print "We'll try and combine all the comics in the directory."
else:
	print 'Please provide just 1 file name'
	quit()

print 'Getting things ready'
print ''

if os.path.exists(downDir):
	shutil.rmtree(downDir)

os.makedirs(downDir)

for filename in os.listdir('.'):
	if filename.endswith('.cbr') or filename.endswith('.cbz'):
		if 'startNum' in globals():
			if counter >= startNum and counter <= endNum:
				extractComic(filename)	
		else:
			extractComic(filename)
		counter += 1

print ''
print 'Creating: ' + sys.argv[1]

target_dir = downDir
zip = zipfile.ZipFile(sys.argv[1], 'w', zipfile.ZIP_DEFLATED, allowZip64=True)
rootlen = len(target_dir) + 1
for base, dirs, files in os.walk(target_dir):
	for file in files:
		print 'Adding ' + '"' + file + '"'
		fn = os.path.join(base, file)
		zip.write(fn, fn[rootlen:])

print ''
print sys.argv[1] + ' completed. Cleaning up'

shutil.rmtree(downDir)